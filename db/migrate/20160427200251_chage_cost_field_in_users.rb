class ChageCostFieldInUsers < ActiveRecord::Migration
  def change
    change_column :users, :cost, :decimal, :null => true, default: nil
  end
end
