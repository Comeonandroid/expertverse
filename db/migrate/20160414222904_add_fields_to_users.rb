class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :availability, :string
    add_column :users, :timezone, :string
    add_column :users, :phone, :string
  end
end
