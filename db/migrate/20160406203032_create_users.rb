  class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :firstname
      t.string :lastname
      t.string :position
      t.string :role
      t.text   :description
      t.text   :keywords      
      t.integer :rating
      t.decimal :cost

      t.timestamps null: false
    end
  end
end
