class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.integer :user_id
      t.integer :expert_id
      t.string :timezone
      t.text :availability
      t.string :phone
      t.text :info

      t.timestamps null: false
    end
  end
end
