ActiveRecord::Base.class_eval do
  def self.process *fields, action, on: :save
    send "before_#{on}" do
      fields.each do |field|
        if action.is_a? Proc
          send "#{field}=", action.call(send field)
        else
          send "#{field}=", send(field).send(action)
        end
      end
    end
  end
end
