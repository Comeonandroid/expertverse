require 'rails_helper'
require 'shared_contexts'

describe "USERS" do

  describe "SIGN UP" do

    describe "GET /users/sign_up" do
      it 'should be 200' do
        get "/users/sign_up"
        expect(response.status).to eq 200
      end
    end

    describe "POST /users" do
      context 'valid:' do
        it 'should create new user' do
          user = build(:user).attributes

          request_headers = {
              "Accept" => "application/json",
              "Content-Type" => "application/json"
            }

          user['password'] = 'password'
          user['password_confirmation'] = 'password'
          json = { user: user }.to_json

          post "/users", json, request_headers

          expect(response).to be_success

          new_user = User.order('id').last
          expect(user['email']).to eq new_user.email
          expect(user['role']).to eq new_user.role.to_s
          expect(new_user.role.to_s).to eq "user"
        end
      end
      context 'invalid:' do
        it 'should NOT create new user' do
          user = build(:user).attributes

          request_headers = {
              "Accept" => "application/json",
              "Content-Type" => "application/json"
            }

          user['email']  = ""
          user['password'] = 'password'
          user['password_confirmation'] = 'password'
          json = { user: user }.to_json

          post "/users", json, request_headers

          expect(response).not_to be_success

          new_user = User.order('id').last
          expect(new_user).to eq nil
        end
      end
    end
  end

  describe "SIGN IN" do
    describe "GET /users/sign_in" do
      it 'should be 200' do
        get "/users/sign_in"
        expect(response.status).to eq 200
      end
    end
    describe "POST /users/sign_in" do
      context 'valid:' do
        it 'should create new session' do
          user = create(:user)

          new_user = User.order('id').last
          expect(new_user.email).to eq user.email

          request_headers = {
              "Accept" => "application/json",
              "Content-Type" => "application/json"
            }
          request = { user:
            {
            email: user.email,
            password: 'password'
            }
          }
          json = request.to_json

          post "/users/sign_in", json, request_headers

          expect(response).to be_success
        end
      end
      context 'invalid:' do
        it 'should NOT create new session' do
          user = create(:user)

          new_user = User.order('id').last
          expect(new_user.email).to eq user.email

          request_headers = {
              "Accept" => "application/json",
              "Content-Type" => "application/json"
            }
          request = { user:
            {
            email: user.email,
            password: 'password123'
            }
          }
          json = request.to_json

          post "/users/sign_in", json, request_headers

          expect(response).not_to be_success
        end
      end
    end
  end

  describe "RESET PASSWORD" do
    describe "GET /users/password/new" do
      it 'should be 200' do
        get "/users/password/new"
        expect(response.status).to eq 200
      end
    end
    describe "GET /users/password/edit" do
      it 'should be 200' do
        get "/users/password/new"
        expect(response.status).to eq 200
      end
    end
  end

  describe "CHANGE PASSWORD" do
    context "guest" do
      describe "GET /users/new_password" do
        it 'redirects to root' do
          get "/users/new_password"
          expect(response).should redirect_to root_path
        end
      end
    end

    context "user" do
      let(:user) { create(:user) }

      describe "GET /users/new_password" do
        it 'should be 200' do
          login_as(user, scope: :user)
          get "/users/new_password"
          expect(response.status).to eq 200
        end
      end



      describe "PATCH /users/update_password" do
        context 'valid:' do
          it 'should update user password' do
            login_as(user, scope: :user)
            request_body = { user:
              {
                current_password: "password",
                password: "password123",
                password_confirmation: "password123"
              }
            }

            request_headers = {
                "Accept" => "application/json",
                "Content-Type" => "application/json"
              }

            patch "/users/update_password", request_body.to_json, request_headers

            expect(response).to be_success

          end
        end

        context 'invalid:' do
          it 'should NOT update user password with wrong current_password' do
            login_as(user, scope: :user)
            request_body = { user:
              {
                current_password: "password321",
                password: "password",
                password_confirmation: "password"
              }
            }

            request_headers = {
                "Accept" => "application/json",
                "Content-Type" => "application/json"
              }

            patch "/users/update_password", request_body.to_json, request_headers

            expect(response).to_not be_success
          end
        end

        it 'should NOT update user password with wrong password_confirmation' do
          login_as(user, scope: :user)
          request_body = { user:
            {
              current_password: "password",
              password: "password123",
              password_confirmation: "password321"
            }
          }

          request_headers = {
              "Accept" => "application/json",
              "Content-Type" => "application/json"
            }

          patch "/users/update_password", request_body.to_json, request_headers

          expect(response).to_not be_success
        end
      end
    end
  end

  describe "PROFILE" do

    shared_examples "public access" do
      describe "GET /profile" do
        it 'should redirect to root' do
          get "/profile"
          expect(response).should redirect_to root_path
        end
      end
    end

    shared_examples "private access" do
      describe "GET /profile" do
        it 'should be 200' do
          login_as(user, scope: :user)
          get "/profile"
          expect(response.status).to eq 200
        end
      end
      describe "GET /users/:id/edit/" do
        it 'should be 200' do
          login_as(user, scope: :user)
          get "/users/#{user.id}/edit"
          expect(response.status).to eq 200
        end
      end
    end

    context "guest" do
      it_behaves_like "public access"
    end

    context "user" do
      let(:user) { create(:user) }
      it_behaves_like "private access"
    end

    context "expert" do
      let(:user) { create(:expert) }
      it_behaves_like "private access"
    end
  end


  describe "NEW EXPERT" do

    shared_examples "public access" do
      describe "GET /new-expert" do
        it 'should redirect to root' do
          get "/new-expert"
          expect(response).should redirect_to new_user_registration_path
        end
      end
    end


    context "guest" do
      it_behaves_like "public access"
    end

    context "user" do
      let(:user) { create(:user) }
      describe "GET /new-expert" do
        it 'should be 200' do
          login_as(user, scope: :user)
          get "/new-expert"
          expect(response.status).to eq 200
        end
      end

      describe "PATCH /users/create_expert" do
        context "valid: " do
          context "format.html" do
            it 'should create new expert' do
              user = create(:new_expert)
              login_as(user, scope: :user)

              query = { user: user.attributes }.to_query

              patch "/users/create_expert", query

              expect(response.status).to eq 302

              new_expert = User.find(user.id)
              expect(user['email']).to eq new_expert.email
              expect(new_expert.role.to_s).to eq "expert"
            end
          end

        context "format.json" do
          it 'should create new expert ' do
            user = create(:new_expert)
            login_as(user, scope: :user)
            request_headers = {
                "Accept" => "application/json",
                "Content-Type" => "application/json"
              }


            json = { user: user.attributes }.to_json

            patch "/users/create_expert", json, request_headers

            expect(response.status).to eq 200

            new_expert = User.find(user.id)
            expect(user['email']).to eq new_expert.email
            expect(new_expert.role.to_s).to eq "expert"
          end
        end
      end

      context "invalid: " do
          context "format.html" do
            it 'should NOT create new expert' do
              user = create(:new_expert)
              login_as(user, scope: :user)

              user.keywords = nil
              query = { user: user.attributes }.to_query

              patch "/users/create_expert", query

              expect(response.status).to eq 200

              new_expert = User.find(user.id)
              expect(user['email']).to eq new_expert.email
              expect(new_expert.role.to_s).to eq "user"
            end
          end

        context "format.json" do
          it 'should NOT create new expert ' do
            user = create(:new_expert)
            login_as(user, scope: :user)
            request_headers = {
                "Accept" => "application/json",
                "Content-Type" => "application/json"
              }

            user.keywords = nil
            json = { user: user.attributes }.to_json

            patch "/users/create_expert", json, request_headers

            expect(response.status).to eq 422

            new_expert = User.find(user.id)
            expect(user['email']).to eq new_expert.email
            expect(new_expert.role.to_s).to eq "user"
          end
        end
      end

    end
  end
    context "expert" do
      let(:user) { create(:expert) }
      it_behaves_like "public access"
    end
  end
end
