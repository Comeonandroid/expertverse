require 'rails_helper'
require 'shared_contexts'

describe "REQUESTS" do
  describe "NEW REQUEST" do
    let(:expert){create(:expert)}
    let(:user){create(:user)}
    describe "GET /users/:user_id/requests/new" do
      context "guest" do
        it 'should redirects to sign up' do
          get "/users/#{expert.id}/requests/new"
          expect(response).should redirect_to new_user_registration_path
        end
      end
      context "user" do
        it 'should be 200' do
          login_as(user, scope: :user)
          get "/users/#{expert.id}/requests/new"
          expect(response.status).to eq 200
        end
      end
    end

    describe "POST /users/:user_id/requests" do
      context "user" do
        context "format.html" do
          it 'should create new request' do
            login_as(user, scope: :user)
            request = create(:request)
            request.user_id = user.id

            query = { request: request.attributes }.to_query
            post "/users/#{expert.id}/requests", query

            expect(response.status).to eq 200

            new_request = Request.order('id').last
            expect(new_request.user_id).to eq user.id
            expect(new_request.expert_id).to eq expert.id
            expect(new_request.phone).to eq request.phone
          end
        end
        context "format.json" do
          it 'should create new request' do
            login_as(user, scope: :user)
            request = create(:request)
            request.user_id = user.id

            request_headers = {
              "Accept" => "application/json",
              "Content-Type" => "application/json"
            }

            json = { request: request.attributes }.to_json
            post "/users/#{expert.id}/requests", json, request_headers

            expect(response.status).to eq 200

            new_request = Request.order('id').last
            expect(new_request.user_id).to eq user.id
            expect(new_request.expert_id).to eq expert.id
            expect(new_request.phone).to eq request.phone
          end
        end
      end
    end
  end
end
