FactoryGirl.define do
  factory :user do
    email Faker::Internet.email
    password "password"
    password_confirmation "password"
    firstname Faker::Name.first_name
    lastname Faker::Name.last_name
    role :user
    factory :expert do
      email Faker::Internet.email
      password "password"
      password_confirmation "password"
      firstname Faker::Name.first_name
      lastname Faker::Name.last_name
      keywords Faker::Lorem.paragraph(2)
      position Faker::Lorem.paragraph(2)
      rating Faker::Number.number(2)
      cost Faker::Number.decimal(2)
      description Faker::Lorem.paragraph(2)
      role :expert
    end
    factory :new_expert do
      keywords Faker::Lorem.paragraph(2)
      position Faker::Lorem.paragraph(2)
      rating Faker::Number.number(2)
      cost Faker::Number.decimal(2)
      description Faker::Lorem.paragraph(2)
    end
  end
end
