FactoryGirl.define do
  factory :request do
    user_id 1
    expert_id 1
    timezone Faker::Lorem.paragraph(2)
    availability Faker::Lorem.paragraph(2)
    phone Faker::PhoneNumber
    info Faker::Lorem.paragraph(2)
  end
end
