require 'rails_helper'

describe Request do

  describe "has all the required fields" do
    it {should have_db_column :user_id}
    it {should have_db_column :expert_id}
    it {should have_db_column :timezone}
    it {should have_db_column :availability}
    it {should have_db_column :phone}
    it {should have_db_column :info}
    it {should have_db_column :created_at}
  end

  describe "has all the required validations" do
    it {should validate_presence_of :phone}
    it {should validate_presence_of :timezone}
    it {should validate_presence_of :availability}
  end

  describe "has all the required relations" do
    it { should belong_to :user }
    it { should belong_to :expert }
  end

  it 'has a valid factory' do
    expect(build(:request)).to be_valid
  end
end
