require 'rails_helper'

describe User do

  describe "has all the required fields" do
    it {should have_db_column :email}
    it {should have_db_column :firstname}
    it {should have_db_column :lastname}
    it {should have_db_column :keywords}
    it {should have_db_column :position}
    it {should have_db_column :role}
    it {should have_db_column :description}
    it {should have_db_column :rating}
    it {should have_db_column :cost}
    it {should have_db_column :timezone}
    it {should have_db_column :phone}
  end

  describe "has all the required validations" do
    it {should validate_presence_of :email}
    it {should validate_presence_of :firstname}
    it {should validate_presence_of :lastname}
    it {should validate_numericality_of :cost}
    it {should validate_numericality_of :rating}
  end

  it 'has a valid factory' do
    expect(build(:user)).to be_valid
    expect(build(:expert)).to be_valid
  end

  it 'has a user role' do
    user = create(:user)
    expect(user.role).to eq :user
  end

  it 'has an expert role' do
    user = create(:expert)
    expect(user.role).to eq :expert
  end

  describe ".fullname" do
    it 'should return fullname of user' do
      user = create(:user)
      expect(user.fullname).to eq user.firstname + ' ' + user.lastname
    end
  end
end
