ActiveAdmin.register User do

  menu priority: 1


  permit_params :email, :firstname, :lastname, :role,:timezone, :availability, :position, :description, :cost, :rating, :keywords, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :email
    column :fullname
    column :role
    column :created_at
    actions
  end

  filter :email
  filter :firstname
  filter :lastname
  filter :role
  filter :position
  filter :description
  filter :cost
  filter :rating
  filter :keywords
  filter :updated_at
  filter :created_at

  form do |f|
    f.inputs "User info" do
      f.input :email
      f.input :firstname
      f.input :lastname
      f.input :role
      f.input :description
      if user.expert?
        f.input :position
        f.input :cost
        f.input :rating
        f.input :keywords
      else
        f.input :timezone
        f.input :availability
      end
    end
    f.actions
  end
  show do
    attributes_table do
      row :id
      row :email
      row :firstname
      row :lastname
      row :role
      row :description
      if user.expert?
        row :position
        row :cost
        row :rating
        row :keywords
      else
        row :timezone
        row :availability
      end
      row :updated_at
      row :created_at
    end
  end

end
