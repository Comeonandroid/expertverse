ActiveAdmin.register Request, :as => "Expert Requests" do

  menu priority: 1

  permit_params :user_id, :expert_id, :phone, :timezone, :availability, :info

  index do
    selectable_column
    id_column
    column :user
    column :expert
    column :phone
    column :created_at
    actions
  end


  form do |f|
    f.inputs "Request info" do
      f.input :user
      f.input :expert
      f.input :phone
      f.input :timezone
      f.input :availability
      f.input :info
    end
    f.actions
  end
  show do
    attributes_table do
      row :id
      row :user
      row :expert
      row :phone
      row :timezone
      row :availability
      row :info
      row :updated_at
      row :created_at
    end
  end

end
