ThinkingSphinx::Index.define :user, :with => :active_record, :delta => true do
  # fields
  indexes keywords
  indexes firstname
  indexes lastname
  indexes role

  # attributes
  has created_at, updated_at
end
