$(document).on("ajax:success","form.message-form", (event, jqxhr, settings, exception) ->
  $('.messages-cont').prepend(jqxhr.html)
  $('.messages-cont .chat-message').first().hide()
  $('.messages-cont .chat-message').first().fadeIn(300)
  $(this)[0].reset()
  user_id = $(this).data('user-id')
  message_store.clearMessage(user_id)
).on("ajax:error","form.message-form", (event, jqxhr, settings, exception) ->
  #$(event.currentTarget).render_form_errors($.parseJSON(jqxhr.responseText))
)

$(document).on("ajax:success","form.message-modal-form", (event, jqxhr, settings, exception) ->
  $(this).closest('.remodal').remodal().close()
  alerts.show(jqxhr.notice)
  user_id = $(this).data('user-id')
  message_store.clearMessage(user_id)
).on("ajax:error","form.modal-form", (event, jqxhr, settings, exception) ->
  $(event.currentTarget).render_form_errors($.parseJSON(jqxhr.responseText))
)

$(document).on 'keyup','#message_body',(e)->
  message = $(this).val()
  user_id = $(this).closest('form').data('user-id')
  message_store.saveMessage(user_id,message)
