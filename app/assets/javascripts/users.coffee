$ ->
  $(document).on("ajax:success","form#new_user", (event, jqxhr, settings, exception) ->
    window.location.href = '/'
  ).on("ajax:error","form#new_user", (event, jqxhr, settings, exception) ->
    console.log jqxhr.responseText
    $(event.currentTarget).render_form_errors($.parseJSON(jqxhr.responseText).errors)
  )


  $(document).on("ajax:success","form#login_user", (event, jqxhr, settings, exception) ->
    window.location.href = '/'
  ).on("ajax:error","form#login_user", (event, jqxhr, settings, exception) ->
    console.log jqxhr
    $(event.currentTarget).render_one_form_errors($.parseJSON(jqxhr.responseText).error)
  )
