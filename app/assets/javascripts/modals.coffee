$(document).on 'click','.modal-trigger',(e)->
  e.preventDefault()
  target = $(this).data('target-id')
  href   = $(this).attr('href')
  $.getJSON href, (data)->
    $('.modal-container').empty()
    $('.remodal-wrapper').remove()
    $('.modal-container').append data.html
    $("[data-remodal-id='#{target}']").remodal().open()

$(document).on("ajax:success","form.modal-form", (event, jqxhr, settings, exception) ->
  $(this).closest('.remodal').remodal().close()
  alerts.show(jqxhr.notice)
).on("ajax:error","form.modal-form", (event, jqxhr, settings, exception) ->
  $(event.currentTarget).render_form_errors($.parseJSON(jqxhr.responseText))
)
