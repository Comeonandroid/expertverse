window.alerts = {
  setTimeout: ()->
    setTimeout ()->
      $('.alert').fadeOut()
    , 3500
  show: (message)->
    $('body').prepend("<div class='alert alert-info alert-dismissible fade in' role='alert'><button class='close' data-dismiss='alert'>x</button>#{message}</div>")
    alerts.setTimeout()
}
