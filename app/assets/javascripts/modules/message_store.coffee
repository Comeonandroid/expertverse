window.message_store = {
  saveMessage: (user_id,message)->
    localStorage.setItem("message_to_#{user_id}",message);
  getMessage: (user_id) ->
    localStorage.getItem("message_to_#{user_id}");
  clearMessage: (user_id)->
    localStorage.removeItem("message_to_#{user_id}");
}
