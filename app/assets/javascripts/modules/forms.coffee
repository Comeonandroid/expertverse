$.fn.render_form_errors = (errors) ->
  @clear_previous_errors()
  model = @data('model')
  form = $(this)

  $.each errors, (field, messages) ->
    $input = form.find('[name="' + model + '[' + field + ']"]')
    $input.addClass('has-error')
    $input.closest('li').prepend "<div class='help-block error-label'><span class='label-arrow'></span>#{messages.join(', ')}</div>"

$.fn.render_one_form_errors = (errors) ->
  @clear_previous_errors()
  this.prepend "<div class='help-block'>#{errors}</div>"

$.fn.clear_previous_errors = ->
  this.find('.has-error').removeClass 'has-error'
  this.find('.help-block').remove()
