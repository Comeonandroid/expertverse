
alerts.setTimeout()

$(document).on 'submit','.search-form', (e)->
  e.preventDefault()
  if $(this).find('#search').val().replace(/\s+/g,"").length > 0
    $(this)[0].submit()

$(document).on 'keyup','#user_cost', (e)->
  return false if e.which == 69

$('li.error').each (el)->
  $(this).first().closest('div').prev('div').addClass('error')


$(document).on("ajax:success","form.default-form", (event, jqxhr, settings, exception) ->
  window.location.href = jqxhr.location
).on("ajax:error","form.default-form", (event, jqxhr, settings, exception) ->
  console.log jqxhr.responseText
  $(event.currentTarget).render_form_errors($.parseJSON(jqxhr.responseText))
)


$(document).on("ajax:success","form.default-single-form", (event, jqxhr, settings, exception) ->
  window.location.href = '/'
).on("ajax:error","form.default-single-form", (event, jqxhr, settings, exception) ->
  $(event.currentTarget).render_one_form_errors($.parseJSON(jqxhr.responseText).error)
)


$(document).on 'change','.ex-avatar-placeholder input[type="file"]', (e)->
  console.log "test"
  input = this
  if input.files && input.files[0]
    reader = new FileReader();
    reader.onload = (e)->
      $(input).closest(".ex-avatar-placeholder").css('background-image', 'url(' + e.target.result + ')')
    reader.readAsDataURL(input.files[0]);

$(document).on 'click','.ex-avatar-placeholder input[type="file"]', (e)->
  e.stopPropagation()

$(document).on 'click','.ex-avatar-placeholder', (e)->
  $(this).find('input[type="file"]').click()
