class Message < ActiveRecord::Base
  belongs_to :conversation, :counter_cache => true
  belongs_to :user
  validates_presence_of :body, :conversation_id, :user_id

  before_save :update_conversation

  default_scope {order(created_at: "DESC")}

  def update_conversation
    self.conversation.update(updated_at: Time.now)
  end

  def message_date
   created_at.strftime("%Y/%m/%d")
  end
  def message_time
   created_at.strftime("%l:%M %p")
  end
end
