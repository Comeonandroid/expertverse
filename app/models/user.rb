class User < ActiveRecord::Base
  include ThinkingSphinx::Scopes
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :sent_requests, class_name: "Request", foreign_key: :user_id
  has_many :received_requests, class_name: "Request", foreign_key: :expert_id

  has_many :experts, through: :sent_requests
  has_many :users, through: :received_requests

  has_many :messages, dependent: :destroy

  validates :firstname, :lastname, :presence => true
  validates :email, length: { maximum: 255 }
  validates :firstname, :lastname, length: { maximum: 50 }
  validates :keywords, length: { maximum: 400 }
  validates :phone, length: { maximum: 20 }
  validates :cost,:rating, :numericality => {:allow_blank => true}

  validates :position,:description, :keywords, :presence => true, if: Proc.new {|u| u.expert?}

  as_enum :role, [:user, :expert],  source: :role,      map: :string

  mount_uploader :avatar, AvatarUploader

  before_create :set_defaults
  after_initialize :init

  after_save ThinkingSphinx::RealTime.callback_for(:user)
  process :cost, -> (val) {val.to_f.round(2)}

  scope :experts, -> {where(role: 'expert').order(created_at: "DESC")}
  scope :users,   -> {where(role: 'user')}

  sphinx_scope(:experts_search) {
    {:conditions => {role: 'expert'}}
  }

  attr_accessor :delta

  def conversations
    Conversation.where("sender_id = ? OR recipient_id = ?", self.id, self.id)
  end

  def set_defaults
    self.role = :user
  end

  def init
    self.cost = nil if self.cost == 0.0
  end

  def fullname
    [firstname, lastname].compact.reject(&:blank?).join(' ')
  end
end
