class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    can :see_contacts, User, :id => user.id

    case user.role
    when :user
      can [:index,:show, :profile, :new_password, :update_password,:new_expert, :create_expert], User
      can [:edit, :update], User, :id => user.id
      can [:new, :create], Request
      can [:new, :create], Message
      can [:index], Conversation
      can [:show], Conversation do |conversation|
        conversation.sender_id == user.id || conversation.recipient_id == user.id
      end
    when :expert
      can [:index,:show, :profile, :new_password, :update_password], User
      can [:edit, :update], User, :id => user.id
      can [:create], Message
      can [:index], Conversation
      can [:show], Conversation do |conversation|
        conversation.sender_id == user.id || conversation.recipient_id == user.id
      end
    else
      can [:index,:show,:new_expert], User
      can [:new], Request
      can [:new], Message
    end
  end
end
