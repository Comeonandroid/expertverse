class Request < ActiveRecord::Base
  belongs_to :user, :foreign_key => :user_id, class_name: 'User'
  belongs_to :expert, :foreign_key => :expert_id, class_name: 'User'

  validates :phone,:timezone,:availability, :presence => true

  validates :phone, length: { maximum: 20 }
end
