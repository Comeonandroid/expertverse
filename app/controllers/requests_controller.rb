class RequestsController < ApplicationController
  before_filter :set_user, only: [:new,:create]
  load_and_authorize_resource

  def new
    if !user_signed_in?
      redirect_to new_user_registration_path, flash: {notice: "Sign up or login to book an expert"}
    end
    @request = Request.new
  end

  def create
    @request = Request.new(request_params.merge(user_id: current_user.id, expert_id: @user.id))
    respond_to do |format|
      if @request.save
        RequestMailer.send_to_admin(@request).deliver
        format.html { render 'success' }
        format.json { render json: { location: new_user_request_path(@user)}, status: 200 }
      else
        format.html { render 'new' }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_user
      @user = User.find(params[:user_id])
    end

    def request_params
      params.require(:request).permit(:timezone,:phone,:availability,:info)
    end
end
