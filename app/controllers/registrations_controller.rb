class RegistrationsController < Devise::RegistrationsController
  respond_to :html, :json
  skip_before_filter :verify_authenticity_token

  def sign_up_params
    devise_parameter_sanitizer.sanitize(:sign_up)
    params.require(:user).permit(:email, :password, :password_confirmation, :firstname, :lastname, :keywords, :position, :role, :description, :rating, :cost)
  end

  protected

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def after_sign_up_path_for(resource)
    session[:previous_url] || root_path
  end
end
