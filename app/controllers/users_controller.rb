class UsersController < ApplicationController
  load_and_authorize_resource param_method: :password_params

  def index
  end

  def show
    @user = get_user    
  end

  def profile
    @user = current_user
    render 'show'
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update_attributes(user_params)
        flash[:notice] = "Profile successfully updated"
        format.html { redirect_to profile_path }
        format.json { render json: { location: profile_path}, status: 200 }
      else
        format.html { render 'edit'}
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def new_password
   @user = current_user
  end

  def update_password
    @user = User.find(current_user.id)
    respond_to do |format|
      if password_params[:password].blank? && password_params[:password_confirmation].blank?
        @user.errors.add(:password, "New password must be at least 8 characters")
        format.html { render 'new_password' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      elsif @user.update_with_password(password_params)
        sign_in @user, :bypass => true
        flash[:notice] = "Your password has been changed"
        format.html {redirect_to profile_path}
        format.json { render json: { location: root_path}, status: 200 }
      else
        format.html { render 'new_password' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def new_expert
    if !user_signed_in?
      redirect_to new_user_registration_path, flash: {notice: "Sign up or login to create your expert profile"}
    end
    @user = current_user
  end

  def create_expert
    @user = User.find(current_user.id)
    @user.role = :expert
    respond_to do |format|
      if @user.update_attributes(user_params)
        flash[:notice] = "Expert Profile successfully created"
        format.html { redirect_to profile_path }
        format.json { render json: { location: profile_path}, status: 200 }
      else
        format.html { render 'new_expert'}
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def get_user
      if !params[:id]
        current_user
      else
        User.find(params[:id])
      end
    end

    def password_params
      params.require(:user).permit(:password, :password_confirmation, :current_password)
    end

    def user_params
      params.require(:user).permit(:firstname, :lastname, :avatar, :keywords, :position, :description, :rating, :cost, :phone, :availability, :timezone,:delta)
    end

    private
      def not_authorized
        redirect_to root_url, alert: "Unauthorized Access"
      end
end
