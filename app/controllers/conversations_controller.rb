class ConversationsController < ApplicationController
  #before_filter :set_user, only: [:new,:create]
  load_and_authorize_resource

  def index
    @conversations = current_user.conversations.active.includes(:messages,:sender,:recipient).all
  end

  def show
    @conversation = current_user.conversations.active.includes(:messages,:sender,:recipient).find(params[:id])
    @interlocutor = @conversation.interlocutor(current_user)
    @messages     = @conversation.messages.all
  end
end
