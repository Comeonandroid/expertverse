class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end
  protect_from_forgery with: :exception
  after_filter :store_location

  def store_location
    return unless request.get?
    if (request.path != "/users/sign_in"  &&
        request.path != "/users/sign_up"  &&
        request.path != "/users/password/new"  &&
        request.path != "/users/password/edit" &&
        request.path != "/users/confirmation"  &&
        request.path != "/users/sign_out" &&
        !request.xhr?)
      session[:previous_url] = request.fullpath
    end
  end

end
