class MessagesController < ApplicationController
  before_filter :set_converstion, only: [:new,:create]
  load_and_authorize_resource

  def new
    if !user_signed_in?
      redirect_to new_user_registration_path, flash: {notice: "Sign up or login to message expert"}
    else
      @user = User.find(params[:user_id])
      respond_to do |format|
        format.html {redirect_to user_path(@user)}
        format.json {render json: { html: render_to_string('new', layout: false)}, status: 200 }
      end
    end
  end

  def create
    @message = @conversation.messages.new(message_params.merge(user_id: current_user.id))
    respond_to do |format|
      if @message.save
        format.json { render json: { notice: "Your message has been sent", html: render_to_string('_message', layout: false, locals:{message: @message})}, status: 200 }
      else
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def message_params
      params.require(:message).permit(:body)
    end

    def set_converstion
      if user_signed_in?
        if Conversation.between(current_user.id,params[:user_id]).present?
          @conversation = Conversation.between(current_user.id,params[:user_id]).first
        else
          @conversation = Conversation.create(sender_id: current_user.id, recipient_id: params[:user_id])
        end
      end
    end
end
