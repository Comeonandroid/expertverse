class SearchController < ApplicationController
  def index
    @search = params['search'].strip || nil
    @nothing_found = false
    if !@search.blank?
      @users      = User.experts_search.search(@search.gsub(" ","|")).page(params[:page]).per(params[:per])
      @nothing_found = true if @users.count < 1
    else
      @users      = User.experts.all
    end
  end
end
