class RequestMailer < ApplicationMailer
  def send_to_admin(request)
    @request = request
    mail(subject: 'New Request on Expertverce.co')
  end
end
