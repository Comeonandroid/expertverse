class ApplicationMailer < ActionMailer::Base
  add_template_helper(ApplicationHelper)

  default from: 'info@expertverse.co'
  default to:   'simon@expertverse.co'
  #default to:   'comeonandroid@gmail.com'
  layout 'mailer'
end
