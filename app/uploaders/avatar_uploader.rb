class AvatarUploader < CommonUploader
  include CarrierWave::MiniMagick
  process :resize_to_fill => [350, 350]

  version :thumb do
    process :resize_to_fill => [150, 150]
  end

end
