module ApplicationHelper
  def placeholder_if_empty(string, placeholder = "Blank.")
    if string.blank?
      placeholder
    else
      string
    end
  end

  def sanitaze
    self.gsub(/(<.*?>)/, '')
  end

  def nl2br(text)
    text.gsub(/\n/, '<br />').html_safe
  end

  def bootstrap_class_for flash_type
    #{ success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type] || flash_type.to_s
    "alert-info"
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} alert-dismissible fade in", role: "alert") do
              concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
              concat message
            end)
    end
    nil
  end
end
