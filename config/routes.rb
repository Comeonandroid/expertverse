Rails.application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config
  root 'main#index'

  devise_for :users, controllers: {sessions: 'sessions', registrations: 'registrations'}

  resources :users do
    collection do
      get    'new_password'
      patch  'update_password'
      patch  'create_expert'
    end
    resources :requests, only: [:new, :create]
    resources :conversations, only: [:show]
    resources :messages, only: [:new, :create]
  end

  resources :conversations, only: [:index, :show]

  get 'new-expert', to: 'users#new_expert', as: "new_expert_users"

  get 'profile', to: 'users#profile', as: 'profile'
  get 'search',  to: 'search#index',  as: 'search'


end
