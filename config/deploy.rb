set :application, 'expertverse'
set :repo_url, 'git@bitbucket.org:comeonandroid/expertverse.git'

set :deploy_to, '/home/deploy/expertverse'

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

set :linked_dirs, fetch(:linked_dirs) + %w{public/system public/uploads}

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end

namespace :bower do
  desc 'Install bower'
  task :install do
    on roles :web do
      within release_path do
        execute :rake, 'bower:install CI=true'
      end
    end
  end
end

before 'deploy:compile_assets', 'bower:install'
